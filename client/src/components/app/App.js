import React, { Component } from 'react';
//import logo from './logo.svg';
import helpers from './AppModule.js';
import './App.css';
import constants from './constants.js';

class App extends Component {

  constructor(props) {
    super(props);
    this.state = { count : constants,
      response : 'default'
    };
    this.pause = helpers.pause.bind(this);
    this.restart = helpers.restart.bind(this);
  }

  componentDidMount() {
    this.restart();
    this.callApi()
      .then(res => this.setState({ response: res.express }))
      .catch(err => console.log(err));
  }

  callApi = async () => {
    const response = await fetch('/api/hello');
    const body = await response.json();

    if (response.status !== 200) throw Error(body.message);

    return body;
  };

  render() {
    return (
      <div className="App">
        <header className="App-header"> 
          <h1 className="App-title">Welcome to Timer</h1>
        </header>
        <p className="App-intro">
          To get started, edit {this.props.name} <code>src/components/app/App.js</code> and save to reload.
        </p>
        response: {this.state.response}
        <em><h1>Timer :{this.state.count}</h1></em>
        <button className="pause" onClick={this.pause.bind(this,this.interval)} >Pause Timer</button>
        <button className="restart" onClick={this.restart.bind(this,this.interval)} >resume Timer</button>
      </div>
    );
  }
}

export default App;