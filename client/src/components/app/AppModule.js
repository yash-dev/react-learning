const pause = function (interval) {
	clearInterval(interval);
	delete this.interval;
}

const restart = function () {
	if (this.interval) {
		console.log('stop interval' + this.interval + 'running');
		return;
	}
	this.interval = setInterval(start.bind(this));
}

const start = function () {
	this.setState({count : this.state.count + 1});
}
export default {
	pause: pause,
	restart: restart
};